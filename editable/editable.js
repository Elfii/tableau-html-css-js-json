let data ; 
let tableElem = document.createElement('table');
let filtreButton = document.createElement('input');
let saveButton = document.createElement('button');
let buttonDiv = document.getElementById('right');
saveButton.innerText= "save";
filtreButton.placeholder= "filtre";
buttonDiv.appendChild(filtreButton);
buttonDiv.appendChild(saveButton);
filtreButton.addEventListener("input",filtre);





function createTable(elem){
    let jsonURL = elem.getAttribute('data-source') 
    fetch(jsonURL)
    .then((response)=> response.json())
    .then((lesDatasDuFichier) => {
        data = lesDatasDuFichier 
        tableHeader()
        tableBody()
        });
    elem.appendChild(tableElem)
}


function tableHeader(){
    let unElement = data[0]
    let tr = document.createElement('tr')
    for(let key in unElement){                   // for in parcour les clef
        let th = document.createElement('th')   
        th.innerText = key
        th.onclick = function(){
                if(typeof(unElement[key])=== 'string'){
                    data.sort(function (a, b) {
                        return a[key].localeCompare(b[key]);
                    })}
                    else{
                    data.sort((a, b) => {
                    return a[key] - b[key];   
                });
            }
        tableBody()
            }  
        tr.appendChild(th)
    }
    let thead= document.createElement('thead')
    thead.classList.add('tbl-header')
    thead.appendChild(tr)
    tableElem.appendChild(thead)
    
}


function tableBody(){
    let tbody = document.createElement('tbody')
    for(let obj of data){                           // for of parcour les valeurs
        let tr = document.createElement('tr')
        for(let key in obj){
            let td = document.createElement('td')
            td.contentEditable = true;
            td.innerText = obj[key];
            td.addEventListener('input', e => {
                obj[key] = td.innerText;
            });
            console.log(td);
            tr.appendChild(td)
        }
        tbody.appendChild(tr)
    }
    tableElem.querySelector('tbody')?.remove()
    tableElem.appendChild(tbody)
    saveButton.onclick= function update () {
        let dataJson = JSON.stringify(data)
        fetch('../test/up.php',{
            method: 'post',
            body: dataJson,
        } ) // condition du fetch = adresse pour fetch + option
    }
}
function filtre(e){
    let search = e.target.value
    let lines = document.querySelectorAll('tbody tr')
    for(const tr of lines){
        if(tr.innerText.includes(search)){
            tr.style.display = "";
        }else{
            tr.style.display = "none";
        }
    }
}



// filtre celle qu'on ne veut pas displaynone 